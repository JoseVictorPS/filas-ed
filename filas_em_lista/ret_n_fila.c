#include "filasl.h"

void retira_n_fila(Queue * l, float ret);
// Retira todos os registros com o valor n

int main(void) {
    Queue * l = create_q(); // Cria fila
    int i; // Inicia variável de iteração
    float ins; // Inicia valor que será inserido na fila
    int opera; // Inicia variável que controla operações

    do {
        printf("Digite 1 para inserir na fila, e 0 para parar de inserir:  ");
        scanf("%d", &opera); // Recebe valor para opera
        if(opera) {
            printf("Insira um valor na fila: ");
            scanf("%f", &ins); // Recebe um valor ins
            insert_q(l, ins); // Insere ins na fila
        }
    }while(opera);

    printf("A fila original é: ");
    print_q(l); // Imprime fila original

    float ret; // Valor a ser retirado da fila
    printf("Digite qual valor deseja remover da fila: ");
    scanf("%f", &ret); // Recebe ret

    retira_n_fila(l, ret); // Retira todos os registros com ret

    printf("A fila após as retiradas é: ");
    print_q(l); // Imprime fila após as retiradas

    free_q(l); // Libera fila original
    return 0;
}

void retira_n_fila(Queue * l, float ret){
    List * ant = l->start; // Ponteiro anterior ao da iteração
    List * aux; // Ponteiro auxiliar para a liberação do nó

    if(l->start->next) { // Se a fila tiver mais de um nó
        List * t = l->start->next; // Ponteiro para o segundo nó
        while(t) { // Iteração percorre a fila toda a partir do segundo nó

            if(t->v == ret) { // Se o valor do nó em questão bater com o que desejo retirar
                if(l->end == t) l->end = ant; /* Caso o nó retirado seja o último,
                o último passa a ser o anterior */
                aux = t; // Auxiliar marca o nó
                t = t->next; // Nó da iteração avança para o próximo
                free(aux); // Libera nó marcado pelo auxiliar
                ant->next = t; // Amarra os nós após a liberação do nó desejado
            }
            else {
                t = t->next; // Atualiza iteração
                ant = ant->next; // Atualiza o ponteiro anterior para o próximo
            }

        }
    }

    ant = l->start; // Reinicia o ponteiro anterior para o começo da fila
    if(ant->v == ret) { // Se o primeiro nó precisa ser retirado
        if(l->end == ant) l->end = NULL; /* Se só houver um nó na fila, após a retirada, a fila
        ficará vazia*/
        l->start = ant->next; // Ponteiro para o começo passa a apontar para o segundo
        free(ant); // Libera o primeiro nó
    }

}