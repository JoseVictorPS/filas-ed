#include "filasl.h"

void void_fila_inverte(Queue * l);
// A função deve inverter a fila sem user de outra fila auxiliar
int count_q(Queue * p);

int main(void) {
    Queue * l = create_q(); // Cria fila
    int i; // Inicia variável de iteração
    float ins; // Inicia valor que será inserido na fila
    int opera; // Inicia variável que controla operações

    do {
        printf("Digite 1 para inserir na fila, e 0 para parar de inserir:  ");
        scanf("%d", &opera); // Recebe valor para opera
        if(opera) {
            printf("Insira um valor na fila: ");
            scanf("%f", &ins); // Recebe um valor ins
            insert_q(l, ins); // Insere ins na fila
        }
    }while(opera);

    printf("A fila original é: ");
    print_q(l); // Imprime fila antes de inverter

    void_fila_inverte(l); // Inverte a fila original

    printf("A fila invertida é: ");
    print_q(l); // Imprime a fila invertida

    free_q(l); // Libera fila 
    return 0;
}

void void_fila_inverte(Queue * l) {
    int c = count_q(l); // Retorna número de elementos na fila
    
    float temp; // Variável auxiliar nas trocas
    List*t; // Inicia nó para iteração
    for(int i = c-1; i>0; i--) { // Itera marcando último número que deve ser trocado
        t = l->start; // Reinicia nó de iteração para o primeiro da fila, a cada iteração
        for(int j=0; j<i; j++) { // Itera desde o início até o último número que deve ser trocado
            temp = t->v; // Variável temporária recebe valor do nó atual
            t->v = t->next->v; // Nó atual recebe valor do próximo
            t->next->v = temp; // Nó próximo recebe valor da variável auxiliar
            t = t->next; // Nó é atualizado para o próximo
        }
    }
    
    return; // Retorna para onde a função foi chamada
}

int count_q(Queue * p) {
    int c = 0; // Inicia contador em 0
    for(List*h = p->start; h; h = h->next)c++; // Para cada nó, incrementa o contador
    return c; // Retorna o contador
}

