#include "filasl.h"

void ret_end_q(Queue * l);

int main(void) {
    Queue * l = create_q(); // Cria fila
    int i; // Inicia variável de iteração
    float ins; // Inicia valor que será inserido na fila
    int opera; // Inicia variável que controla operações

    do {
        printf("Digite 1 para inserir na fila, e 0 para parar de inserir:  ");
        scanf("%d", &opera); // Recebe valor para opera
        if(opera) {
            printf("Insira um valor na fila: ");
            scanf("%f", &ins); // Recebe um valor ins
            insert_q(l, ins); // Insere ins na fila
        }
    }while(opera);

    printf("A fila original é: ");
    print_q(l); // Imprime fila original

    int ret; // Quantidade de retiradas do fim da fila
    printf("Digite quantas vezes deseja retirar do fim: ");
    scanf("%d", &ret); // Recebe ret

    for(i=0; i<ret; i++) {
        ret_end_q(l); // Retira do fim da fila a cada iteração
    }

    printf("A fila após as retiradas é: ");
    print_q(l); // Imprime fila após as retiradas

    free_q(l); // Libera fila original
    return 0;
}

void ret_end_q(Queue * l) {
    if(l->start->next != NULL) { // Se a fila tiver mais de um nó
        List * ant = l->start; // Ponteiro anterior ao da iteração
        List * t = l->start->next; // Ponteiro para o segundo nó
        while(t->next) { // Iteração percorre a fila até o último nó
            t = t->next; // Adianta o ponteiro t
            ant = ant->next; // Adianta o ponteiro ant
        }
        free(t); // Libera o último nó
        l->end = ant; // Campo final passa a apontar para o anterior
        ant->next = NULL; // Campo próximo em ant deixa de apontar para t
    }

    else if (l->start != NULL) { // Se só houver um nó na fila
        List * t = l->start; // Ponteiro para o único nó
        free(t); // Libera o único nó
        l->start = NULL; // Fila fica vazia
        l->end = NULL; // Fila fica vazia
    }

    else if (l->start == NULL) printf("Fila vazia!\n"); // Caso a fila esteja vazia
}