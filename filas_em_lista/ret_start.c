#include "filasl.h"

int main(void) {
    Queue * l = create_q(); // Cria fila
    int i; // Inicia variável de iteração
    float ins; // Inicia valor que será inserido na fila
    int opera; // Inicia variável que controla operações

    do {
        printf("Digite 1 para inserir na fila, e 0 para parar de inserir:  ");
        scanf("%d", &opera); // Recebe valor para opera
        if(opera) {
            printf("Insira um valor na fila: ");
            scanf("%f", &ins); // Recebe um valor ins
            insert_q(l, ins); // Insere ins na fila
        }
    }while(opera);

    printf("A fila original é: ");
    print_q(l); // Imprime fila original

    int ret; // Quantidade de retiradas do início da fila
    printf("Digite quantas vezes deseja retirar do início: ");
    scanf("%d", &ret); // Recebe ret

    float v; // Valor que recebe registro retirado da fila
    for(i=0; i<ret; i++) {
        v = withdraw_q(l); // Retira do início da fila a cada iteração
    }

    printf("A fila após as retiradas é: ");
    print_q(l); // Imprime fila após as retiradas

    free_q(l); // Libera fila original
    return 0;
}