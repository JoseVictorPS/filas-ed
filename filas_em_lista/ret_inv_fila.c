#include "filasl.h"

Queue * ret_fila_inverte(Queue * l);
/* A função trabalhada retornará uma fila invertida, e não
alterará a função original */
void head_insert_q(Queue * p, float v);

int main(void) {
    Queue * l = create_q(); // Cria fila
    int i; // Inicia variável de iteração
    float ins; // Inicia valor que será inserido na fila
    int opera; // Inicia variável que controla operações

    do {
        printf("Digite 1 para inserir na fila, e 0 para parar de inserir:  ");
        scanf("%d", &opera); // Recebe valor para opera
        if(opera) {
            printf("Insira um valor na fila: ");
            scanf("%f", &ins); // Recebe um valor ins
            insert_q(l, ins); // Insere ins na fila
        }
    }while(opera);

    printf("A fila original é: ");
    print_q(l); // Imprime fila original

    Queue * p = ret_fila_inverte(l); // Inicializa e recebe a fila invertida

    printf("A fila invertida é: ");
    print_q(p); // Imprime a fila invertida

    free_q(l); // Libera fila original
    free_q(p); // Libera fila invertida
    return 0;
}

Queue * ret_fila_inverte(Queue * l) {
    Queue * p = create_q(); // Inicia uma fila

    List * q = l->start; // Inicia nó para iteração
    while(q) {
        head_insert_q(p,q->v); // Insere no começo de p, o valor que está em q
        q = q->next; // Atualiza q para o próximo
    }

    return p; // Retorna a fila p
}

void head_insert_q(Queue * p, float v) {
    List * n = (List *)malloc(sizeof(List)); // Aloca espaço para o nó
    n->next = p->start; // Campo próximo do nó, aponta para o primeiro da fila
    n->v = v; // Nó recebe o valor v
    p->start = n; // Começo da fila passa a apontar para o nó n
    if(empty_q(p)) {
        p->end = n; // Caso a fila esteja vazia, o fim também será o nó
    }
}