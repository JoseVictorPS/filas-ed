#include <stdio.h>
#include <stdlib.h>

typedef struct list {
    float v; // Valor que o nó carregará
    struct list * next; // Ponteiro para o próximo nó
}List;

typedef struct queue {
    List * start; // Aponta para o nó inicial
    List * end; // Aponta para o nó fim
}Queue;

Queue * create_q (void);
void insert_q( Queue * f, float v);
float withdraw_q(Queue * f);
int empty_q(Queue * f);
void free_q(Queue * f);
void print_q(Queue * f);

Queue * create_q (void) {
    Queue * f = (Queue *) malloc(sizeof(Queue)); // Aloca espaço para a fila
    f->start = f->end = NULL; // Ponteiros de início e fim são anulados
    return f; // Retorna a fila
}

void insert_q( Queue * f, float v) {
    List * n = (List *)malloc(sizeof(List)); // Aloca espaço para o nó
    n->v = v; // Insere valor v no campo v do nó
    n->next = NULL; // Ponteiro para o próximo é anulado
    if(f->end) { // Se existir um fim, ou seja, a fila não está fazia
        f->end->next = n; // O nó que estava no fim, seu campo próximo aponta para n,
        f->end = n; // O ponteiro fim passa a apontar para n
    }
    else { // Caso a fila esteja vazia
        f->start = n; // O começo apontará para n
        f->end = n; // O fim também apontará para n
    }
}

float withdraw_q(Queue * f) {
    List * t; // Inicia ponteiro auxiliar
    float v; // Inicia valor para retornar
    if(empty_q(f)) {
        printf("Fila vazia!\n");
        return; // Retorna para onde a função foi chamada
    }
    t = f->start; // Ponteiro t aponta para o primeiro nó
    v = t->v; // Valor recebe o que está no campo valor do nó t
    f->start = t->next; // Primeiro da fila passa a ser o segundo nó da fila
    if(!f->start) { // Se a fila ficar vazia
        f->end = NULL; // O fim se tornará nulo também
    }
    free(t); // Libera o nó de t, que ocupa a primeira posição na fila
    return v; // Retorna valor que estava no nó
}

int empty_q(Queue * f) {
    return (f->start == NULL); // Retorna se o começo da fila existe ou não
}

void free_q(Queue * f) {
    List * q = f->start; // Cria um nó para iteração, apontando para o começo da fila
    List * t; // Cria um ponteiro auxiliar
    while(q) {
        t = q->next; // Ponteiro auxiliar recebe o próximo de q
        free(q); // Libera q
        q = t; // Atualiza q com o valor de seu próximo, que estava salvo em t
    }
    free(f); // Libera a fila
}

void print_q(Queue * f) {
    List * q; // Inicia ponteiro para iteração
    for (q = f->start; q!=NULL; q = q->next) {
        printf("%.2f  ",q->v); // Imprime o campo v de cada nó da fila
    }
    printf("\n");
}

