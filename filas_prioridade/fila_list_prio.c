#include "filas_l.h"

int main(void){
    Queue * l = create_q(); // Cria fila

    int opera; // Variável para controlar operações de inserção e retirada
    float ins; // Variável para inserir na fila
    int prio; // Variável de prioridade do nó inserido
    List * ret; //Variável para retirar da fila
    do {
        printf("O que deseja fazer?\n");
        printf("1 para inserir na fila, 2 para retirar e 0 para sair: ");
        scanf("%d", &opera); // Recebe opera

        if (opera==1) { // Se deseja inserir
            printf("Qual numero e com que prioridade deseja inserir na fila? ");
            scanf("%f%d", &ins, &prio); // Recebe ins e prio
            insert_prio_q(l, ins, prio); // Insere ins na fila
        }

        else if (opera==2) { // Se deseja retirar
            ret = withdraw_prio_q(l); // ret recebe primeiro elemento da fila
            if (ret) { //Imprime valor retirado da fila se houver
                printf("O atual primeiro da fila e: %.2f  cuja prioridade: %d\n", ret->v, ret->prio);
            } 
        }

        if (!empty_q(l)){
            printf("Sua fila ficou assim:\n");
            print_q(l); // Sempre imprime caso não esteja vazia
        }

    } while(opera); // Enquanto opera for diferente de 0

    free_q(l); // Libera espaço na memória ocupado pela fila
    return 0;
}