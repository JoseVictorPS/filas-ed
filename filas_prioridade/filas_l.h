#include <stdio.h>
#include <stdlib.h>

typedef struct list {
    float v; // Valor que o nó carregará
    struct list * next; // Ponteiro para o próximo nó
    int prio; // Inteiro que representará a prioridade do nó
}List;

typedef struct queue {
    List * start; // Aponta para o nó inicial
    List * end; // Aponta para o nó fim
}Queue;

Queue * create_q (void);
void insert_prio_q( Queue * f, float v, int prio);
List * withdraw_prio_q(Queue * f);
int empty_q(Queue * f);
void free_q(Queue * f);
void print_q(Queue * f);

Queue * create_q (void) {
    Queue * f = (Queue *) malloc(sizeof(Queue)); // Aloca espaço para a fila
    f->start = f->end = NULL; // Ponteiros de início e fim são anulados
    return f; // Retorna a fila
}

void insert_prio_q( Queue * f, float v, int prio) {
    List * n = (List *)malloc(sizeof(List)); // Aloca espaço para o nó
    n->v = v; // Insere valor v no campo v do nó
    n->prio = prio; // Insere prioridade no campo prio do nó
    
    if(empty_q(f)) { // Se lista vazia
        n->next = NULL; // Não há segundo nó
        f->start = n; // O começo é o nó inserido
        f->end = n; // E o fim também será
        return; // Retorna para onde a função foi chamada
    }

    if(f->start->prio <= n->prio) { // Caso o nó inserido seja o de maior prioridade na fila
        n->next = f->start; // O campo próximo do nó inserido apontará para o primeiro da fila
        f->start = n; // O primeiro da fila passa a ser o nó inserido
        return; // Retorna para onde a função foi chamada
    }

    // Só entra aqui se a fila não estiver vazia, ou seja, existir ao menor 1 nó
    List * i = f->start->next; // Inicia nó para iteração no segundo
    List * ant = f->start; // Inicia nó que se amarrará no que desejamos inserir
    if (f->start->next != NULL) { // Se existir o nó que será usado na iteração
        while(i->prio > n->prio) { // Itera até achar um nó com menor prioridade
            i = i->next; // Avança nó a nó
            ant = ant->next; // Avança o nó que amarrará
            if(!i) break; /* Se i passa a pontar para NULL, quebra o loop,
            pois não dá para testar o campo prio de NULL */
        }
    }
    n->next = i; /* O nó inserido terá como próximo o primeiro nó
    com menos prioridade */
    if(n->next == NULL) f->end = n; /* Se o próximo do nó inserido for nulo,
    quer dizer que ele é o último */
    ant->next = n; /* Amarra o último nó de maior prioridade no nó
    que desejamos inserir */

    return; // Retorna para onde a função foi chamada
}

List * withdraw_prio_q(Queue * f) {
    List * t; // Inicia ponteiro auxiliar
    List * ret = (List *)malloc(sizeof(List)); // Inicia nó réplica do primeiro da fila
    if(empty_q(f)) {
        printf("Fila vazia!\n");
        return NULL; // Retorna para onde a função foi chamada com NULL
    }
    t = f->start; // Ponteiro t aponta para o primeiro nó

    ret->prio = t->prio; // Nó réplica recebe a prioridade do nó do começo da fila
    ret->v = t->v; // Nó réplica recebe o valor carregado no primeiro da fila
    ret->next = NULL; // Nó réplica recebe NULL como próximo, pois não estará numa fila

    f->start = t->next; // Primeiro da fila passa a ser o segundo nó da fila
    if(!f->start) { // Se a fila ficar vazia
        f->end = NULL; // O fim se tornará nulo também
    }
    free(t); // Libera o nó de t, que ocupa a primeira posição na fila
    return ret; // Retorna réplica do primeiro da fila
}

int empty_q(Queue * f) {
    return (f->start == NULL); // Retorna se o começo da fila existe ou não
}

void free_q(Queue * f) {
    List * q = f->start; // Cria um nó para iteração, apontando para o começo da fila
    List * t; // Cria um ponteiro auxiliar
    while(q) {
        t = q->next; // Ponteiro auxiliar recebe o próximo de q
        free(q); // Libera q
        q = t; // Atualiza q com o valor de seu próximo, que estava salvo em t
    }
    free(f); // Libera a fila
}

void print_q(Queue * f) {
    List * q; // Inicia ponteiro para iteração
    for (q = f->start; q!=NULL; q = q->next) {
        printf("valor: %.2f  prioridade: %d\n",q->v, q->prio);
        // Imprime o campo v de cada nó da fila
    }
}

