#include "filasv.h"

void retira_n_fila(Fila * l, int N, int ret);
// Retira todos os registros com o valor n
void fila_n_imprime(Fila * l, int N, int * vet);

int main(void) {
    int N; // Inicializa número de vagas na fila
    printf("Quantos elementos você deseja ter na fila? ");
    scanf("%d", &N); // Recebe N

    Fila * l = fila_cria(N); // Cria fila com N posições
    int i; // Inicia variável de iteração
    float ins; // Inicia valor que será inserido na fila

    for(i=0; i<N; i++) {
        printf("Insira um valor na fila: ");
        scanf("%f", &ins); // Recebe um valor ins
        fila_insere(l, ins, N); // Insere ins na fila
    }

    printf("A fila original é: ");
    fila_imprime(l, N); // Imprime fila original

    int ret; // Valor a ser retirado da fila
    printf("Digite qual valor deseja remover da fila: ");
    scanf("%d", &ret); // Recebe ret

    retira_n_fila(l, N, ret); // Retira todos os registros com ret

    fila_libera(l); // Libera fila original
    return 0;
}

void retira_n_fila(Fila * l, int N, int ret){
    int i; // Inicializa variável usada nas iterações

    int vet[N]; // Inicializa vetor com número de posições igual ao da fila
    for(i=0; i < N; i++) vet[i] = 1; // Atribui para todas as posições 1(true)
    /* A ideia é usá-lo para demarcar onde o index precisa ser ignorado(valor 
    removido da fila), e onde ele precisa ser lido */

    for(i = l->ini; i != l->fim; i = (i + 1) % N) {
        if(l->vet[i] == ret) { // Testa se o valor na fila é o que desejo retirar   
            vet[i] = 0; // Altera index para 0(false)
            l->n--; // Decrementa quantidade de registros da fila
        }
    }

    // A iteração acima para de testar no penúltimo
    if(l->vet[i] == ret) { // Testa se o último valor é o que desejo retirar   
        vet[i] = 0; // Altera index para 0(false)
        l->n--; // Decrementa quantidade de registros da fila
    }

    printf("A fila após as retiradas é: ");
    fila_n_imprime(l, N, vet); // Imprime fila após as retiradas
}

void fila_n_imprime(Fila * l, int N, int * vet) {
    int i; // Inicia variável de iteração
    for(i = l->ini; i != l->fim; i = (i + 1) % N) {
        if(vet[i]) printf("%.2f  ", l->vet[i]);
        // Testa se o valor na fila está presente e o imprime
    }

    // A iteração acima para de testar no penúltimo
    if(vet[i]) printf("%.2f  ", l->vet[i]);
    // Testa se o último valor está presente e o imprime

    printf("\n");
}