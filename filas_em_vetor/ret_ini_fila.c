#include "filasv.h"

int main(void) {
    int N; // Inicializa número de vagas na fila
    printf("Quantos elementos você deseja ter na fila? ");
    scanf("%d", &N); // Recebe N

    Fila * l = fila_cria(N); // Cria fila com N posições
    int i; // Inicia variável de iteração
    float ins; // Inicia valor que será inserido na fila

    for(i=0; i<N; i++) {
        printf("Insira um valor na fila: ");
        scanf("%f", &ins); // Recebe um valor ins
        fila_insere(l, ins, N); // Insere ins na fila
    }

    printf("A fila original é: ");
    fila_imprime(l, N); // Imprime fila original

    int ret; // Quantidade de retiradas do início da fila
    printf("Digite quantas vezes deseja retirar do início: ");
    scanf("%d", &ret); // Recebe ret

    float v; // Valor que recebe registro retirado da fila
    for(i=0; i<ret; i++) {
        v = fila_retira(l, N); // Retira do início da fila a cada iteração
    }

    printf("A fila após as retiradas é: ");
    fila_imprime(l, N); // Imprime fila após as retiradas

    fila_libera(l); // Libera fila original
    return 0;
}