#include "filasv.h"

void void_fila_inverte(Fila * l, int N);
/* A função trabalhada alterará a função original com uso de
recursão invertendo-a e sem auxílio de uma outra fila */

int main(void) {
    int N; // Inicializa número de vagas na fila
    printf("Quantos elementos você deseja ter na fila? ");
    scanf("%d", &N); // Recebe N

    Fila * l = fila_cria(N); // Cria fila com N posições
    int i; // Inicia variável de iteração
    float ins; // Inicia valor que será inserido na fila

    for(i=0; i<N; i++) {
        printf("Insira um valor na fila: ");
        scanf("%f", &ins); // Recebe um valor ins
        fila_insere(l, ins, N); // Insere ins na fila
    }

    printf("A fila original é: ");
    fila_imprime(l, N); // Imprime fila original

    int ini, fim; // Inicializa variáveis para guardar início e fim
    ini = l->ini; // Guarda início da fila
    fim = l->fim; // Guarda fim da fila

    void_fila_inverte(l, N); // Altera a fila original, invertendo-a

    l->ini = ini; // Recebe o valor de início de antes da inversão
    l->fim = fim; // Recebe o valor de fim de antes da inversão

    printf("A fila invertida é: ");
    fila_imprime(l, N); // Imprime a fila invertida

    fila_libera(l); // Libera a fila
    return 0;
}

/* Durante a operação recursiva de inversão, os valores de início e fim são
alterados, precisando assim, serem corrigidos após */
void void_fila_inverte(Fila * l, int N) {
    if(l->ini == l->fim) return; /* Se o início e o fim forem iguais, não
    há mais o que inverter */

    float troca; // Inicializa variável auxiliar a troca
    troca = l->vet[l->ini]; // Variável troca recebe o valor no início
    l->vet[l->ini] = l->vet[l->fim]; // Valor do início recebe o valor do fim
    l->vet[l->fim] = troca; // Valor do fim recebe o valor da variável troca

    l->ini = (l->ini + 1) %N; // Incrementa uma unidade ao index de início
    l->fim = (l->fim - 1 + N) %N; // Decrementa uma unidade ao index de fim

    void_fila_inverte(l, N); // Chama a própria função
}
