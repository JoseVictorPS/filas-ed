#include "filasv.h"

void fim_retira_fila(Fila * l, int N, int ret);

int main(void) {
    int N; // Inicializa número de vagas na fila
    printf("Quantos elementos você deseja ter na fila? ");
    scanf("%d", &N); // Recebe N

    Fila * l = fila_cria(N); // Cria fila com N posições
    int i; // Inicia variável de iteração
    float ins; // Inicia valor que será inserido na fila

    for(i=0; i<N; i++) {
        printf("Insira um valor na fila: ");
        scanf("%f", &ins); // Recebe um valor ins
        fila_insere(l, ins, N); // Insere ins na fila
    }

    printf("A fila original é: ");
    fila_imprime(l, N); // Imprime fila original

    int ret; // Quantidade de retiradas do fim da fila
    printf("Digite quantas vezes deseja retirar do fim: ");
    scanf("%d", &ret); // Recebe ret

    fim_retira_fila(l, N, ret); // Retira os ret últimos registros da fila

    printf("A fila após as retiradas é: ");
    fila_imprime(l, N); // Imprime fila após as retiradas

    fila_libera(l); // Libera fila original
    return 0;
}

void fim_retira_fila(Fila * l, int N, int ret){
    for(int i=0; i<ret; i++) {
        if(!fila_vazia(l)){ // Retira apenas se houver algo na fila
            l->fim = (l->fim - 1 + N) %N; // Decrementa index do fim
            l->n--; // Decrementa quantidade de registros da fila
        }
    }
}