#include <stdio.h>
#include <stdlib.h>

/* int N, como parametro nas funções, é o valor que o usuário deseja ter
de elementos na fila */

typedef struct fila {
    int n; //Elementos na fila
    int ini; // Index do início
    int fim; //Index do fim
    float *vet; //Ponteiro pra vetor de float
}Fila;


Fila * fila_cria(int N);
void fila_insere(Fila * f, float v, int N);
float fila_retira(Fila * f, int N);
int fila_vazia(Fila*f);
void fila_libera(Fila * f);
void fila_imprime(Fila * f, int N);


Fila * fila_cria(int N) {
    Fila * f = (Fila*)malloc(sizeof(Fila)); //Aloca ponteiro pra struct Fila
    f->n = 0; //Inicializa com 0 elementos na fila
    f->ini = 0; // Index início é 0
    f->fim = 0; //Index do fim também é 0
    f->vet = (float*)malloc(sizeof(float)*N);  // Aloca vetor de N posições
}

/* (f->ini + X) % N ---- Este método de incremento é feito pela seguinte maneira:
    f->ini + X, é o index inicial somado as posições que você deseja somar, se
f->ini + 1, você retorna o index próximo ao início; se f->ini + f->n,você
retorna o index início somado as próximas n posições(passando por todos
os elementos da fila, chegando assim ao fim).
    Problema: Após retirar um elemento, a fila fica com a posição início vazia, as
opções para resolver são andar a fila, ou andar o index início. Como andar a fila
pode ser muito custoso, preferi andar o index início.
    Resolução: Ao andar com o index início, numa fila de posições limitadas, é
preciso retornar para a posição 0 para continuar inserindo. Se retira um elemento,
a primeira posição fica vaga, e o início anda até o index do segundo elemento. Ao
inserir um novo elemento, já que todas as outras posições da fila estão vagas, é
necessário utilizar a vaga na primeira posição.
     % N representa "resto da divisão por N", assim se inserir um ele-
mento que passe do index N, o index reseta e passa a contar do 0.
    Exemplo:
             f->ini = 5
             X = 4
             N = 7
             Seu index início é 5, você tem 7 vagas na fila e deseja inserir
             na quarta vaga após o index 5.
             O : Ocupado
             V : Vazio
             O O V V V O O
             (5 + 4) % 7 = 2
             Agora você tem:
             O O O V V O O
*/

void fila_insere(Fila * f, float v, int N) {
    if(f->n==N){ //Teste pra saber se a fila está cheia
        printf("Fila lotada! Operacao NAO efetuada!\n");
        return; //Retorna pra onde a função foi chamada
    }
    //Insere elemento na próxima posição livre
    f->fim = (f->ini + f->n)%N; // index fim é o da próxima posição livre
    f->vet[f->fim] = v; // Insere o valor v no index fim
    f->n++; // Incremento o número de elementos na fila
}

float fila_retira(Fila * f, int N) {
    float v; // Inicializa valor
    if(fila_vazia(f)) { // Checa se fila está vazia
        printf("Fila Vazia! Operacao NAO efetuada!\n");
        return; // Retorna para onde a função foi chamada
    }
    // Retira elemento no início
    v = f->vet[f->ini]; // valor recebe registro da posição retirada
    f->ini = (f->ini + 1) % N; //início recebe próximo index da fila
    f->n--; // Decrementa número de elementos na fila
    return v; // Retorna valor
}

int fila_vazia(Fila*f) {
    return (f->n == 0); //Checa se não há elementos na fila
}

void fila_libera(Fila * f) {
    free(f->vet); // Libera vetor alocado no campo vet
    free(f); // Libera fila
}

void fila_imprime (Fila * f, int N) {
    for(int i=0; i < f->n ; i++ ) // acontece n vezes, para n = qtd de elementos
        printf("%.2f  ", f->vet[ (f->ini+i)%N ]); /* Percerre a partir
        do início iterando pelos i próximos elementos*/
    printf("\n"); // Quebra linha após imprimir a fila toda
}

/* Bibliografia: Introdução a estrutura de dados, de Walder Celes, Renato Cer-
queira e José Lucas Rangel */