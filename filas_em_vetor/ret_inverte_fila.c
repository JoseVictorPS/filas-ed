#include "filasv.h"

Fila * ret_fila_inverte(Fila * l, int N);
/* A função trabalhada retornará uma fila invertida, e não
alterará a função original */

int main(void) {
    int N; // Inicializa número de vagas na fila
    printf("Quantos elementos você deseja ter na fila? ");
    scanf("%d", &N); // Recebe N

    Fila * l = fila_cria(N); // Cria fila com N posições
    int i; // Inicia variável de iteração
    float ins; // Inicia valor que será inserido na fila

    for(i=0; i<N; i++) {
        printf("Insira um valor na fila: ");
        scanf("%f", &ins); // Recebe um valor ins
        fila_insere(l, ins, N); // Insere ins na fila
    }

    printf("A fila original é: ");
    fila_imprime(l, N); // Imprime fila original

    Fila * p = ret_fila_inverte(l, N); // Inicializa e recebe a fila invertida

    printf("A fila invertida é: ");
    fila_imprime(p, N); // Imprime a fila invertida

    fila_libera(l); // Libera fila original
    fila_libera(p); // Libera fila invertida
    return 0;
}

Fila * ret_fila_inverte(Fila * l, int N) {
    Fila * p = fila_cria(N); // Inicia uma fila p com N vagas

    int i; // Inicia variável de iteração
    for(i=0; i < l->n; i++) { // Itera enquanto houver elementos na fila
        p->vet[i] = l->vet[(l->fim - i + N) %N];
        /* pega o elemento na primeira posição de p, e coloca o que está
        na última de l, depois incrementa posição em p, e decrementa a de l 
        e assim por diante.*/
        p->n++; // Incrementa posições de p
        p->fim++; // Incrementa index de fim
    }

    return p; // Retorna a fila p
}
