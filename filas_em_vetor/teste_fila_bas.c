#include "filasv.h"

int main(void){
    int N; // Inicializa número de vagas na fila
    printf("Quantos elementos você deseja ter na fila? ");
    scanf("%d", &N); // Recebe N

    Fila * l = fila_cria(N); // Cria fila com N posições

    int opera; // Variável para controlar operações de inserção e retirada
    float ins; // Variável para inserir na fila
    float ret; //Variável para retirar da fila
    do {
        printf("O que deseja fazer?\n");
        printf("1 para inserir na fila, 2 para retirar e 0 para sair: ");
        scanf("%d", &opera); // Recebe opera

        if (opera==1) { // Se deseja inserir
            printf("Qual numero deseja inserir na fila? ");
            scanf("%f", &ins); // Recebe ins
            fila_insere(l, ins, N); // Insere ins na fila, caso haja espaço
        }

        else if (opera==2) { // Se deseja retirar
            ret = fila_retira(l, N); // ret recebe primeiro elemento da fila
            if (ret) { //Imprime valor retirado da fila se houver
                printf("O atual primeiro da fila e: %.2f\n", ret);
            } 
        }

        if (!fila_vazia(l)){
            printf("Sua fila ficou assim: ");
            fila_imprime(l, N); // Sempre imprime caso não esteja vazia
        }

    } while(opera); // Enquanto opera for diferente de 0

    fila_libera(l); // Libera espaço na memória ocupado pela fila
    return 0;
}